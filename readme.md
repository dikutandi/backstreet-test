# Preparation
- Php 7.0 or earlier
- mysql 

# how to install
- clone repository to your local
- Create Database
- Import backstreet.sql
- Setup DB Configuration in database.php
- define('DB_HOST', '127.0.0.1'); // Database host name (ex. localhost)
- define('DB_USER', 'homestead'); // Database user. (ex. root)
- define('DB_PASSWORD', 'secret'); // user password  (if no password keep it empty )
- define('DB_DATABASE', 'backstreet'); // Database name