<?php
$route = [
    '/'                   => [
        'controller' => 'PagesController',
        'method'     => 'index',
    ],

    '/users'              => [
        'controller' => 'PagesController',
        'method'     => 'users',
    ],

    '/auth/logout'        => [
        'controller' => 'PagesController',
        'method'     => 'logout',
    ],

    '/auth/register'      => [
        'controller' => 'RegisterController',
        'method'     => 'getRegisterForm',
    ],

    '/auth/register/post' => [
        'controller' => 'RegisterController',
        'method'     => 'post',
    ],

    '/auth/login'         => [
        'controller' => 'LoginController',
        'method'     => 'getLoginForm',
    ],

    '/auth/login/post'    => [
        'controller' => 'LoginController',
        'method'     => 'post',
    ],
];
