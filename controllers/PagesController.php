<?php
require 'models/Users.php';
class PagesController
{
    public function __construct()
    {
        if (!isset($_SESSION["login"])) {
            header('Location: ' . '/auth/login');
        }
    }

    public function index()
    {

        $user = $_SESSION['user'];
        $view = false;
        require_once 'views/home.php';

    }

    public function users()
    {

        $user  = $_SESSION['user'];
        $users = Users::getAll();

        $view = 'users.php';
        require_once 'views/home.php';

    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . '/auth/login');
    }

}
