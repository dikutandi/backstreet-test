<?php
require 'models/Users.php';

class RegisterController
{
    protected $errorMessages;

    public function __construct()
    {
        if (isset($_SESSION["login"])) {
            header('Location: ' . '/');
        }
    }

    public function getRegisterForm()
    {
        require_once 'views/auth/register.php';
    }

    public function post()
    {
        $username        = $_POST['username'];
        $email           = $_POST['email'];
        $password        = $_POST['password'];
        $confirmPassword = $_POST['confirm_password'];

        $param = [
            'username'        => $username,
            'email'           => $email,
            'password'        => $password,
            'confirmPassword' => $confirmPassword,
        ];

        $valid = $this->validation($param);
        if ($valid == 0) {
            $_SESSION['error']          = 1;
            $_SESSION['error_messages'] = $this->errorMessages;
            $_SESSION['input']          = $param;
            header('Location: ' . '/auth/register');
        } else {
            unset($param['confirmPassword']);
            $param['password'] = password_hash($param['password'], PASSWORD_DEFAULT);
            $users             = Users::create($param);

            if ($users) {
                $_SESSION['message'] = 'Register Success, Now You Can Login';
                header('Location: ' . '/auth/login');
            } else {
                $_SESSION['error_messages']['default'] = 'Something Error';
                header('Location: ' . '/auth/register');
            }
        }
    }

    /**
     * [validation input]
     * @param  array  $param [username, email, password]
     * @return [type]        [description]
     */
    public function validation($param = [])
    {
        $valid = 1;
        // is username valid
        if ($param['username'] == '') {
            $valid                           = 0;
            $this->errorMessages['username'] = "Required";
        } else {
            $countUsername = Users::getCount('username', $param['username']);
            if ($countUsername > 0) {
                $valid                           = 0;
                $this->errorMessages['username'] = "Username already Exist";
            }
        }

        // is email valid
        if ($param['email'] == '') {
            $valid                        = 0;
            $this->errorMessages['email'] = "Required";
        } else {
            $countEmail = Users::getCount('email', $param['email']);
            if ($countEmail > 0) {
                $valid                        = 0;
                $this->errorMessages['email'] = "Email already Exist";
            }
        }

        // is password valid
        if ($param['password'] == '') {
            $valid                           = 0;
            $this->errorMessages['password'] = "Required";
        } else {
            if ($param['password'] != $param['confirmPassword']) {
                $valid                           = 0;
                $this->errorMessages['password'] = "Confirm Password must be same as password";
            }
        }
        return $valid;
    }

}
