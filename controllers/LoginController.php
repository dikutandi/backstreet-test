<?php

require 'models/Users.php';

class LoginController
{
    public function __construct()
    {
        if (isset($_SESSION["login"])) {
            header('Location: ' . '/');
        }
    }

    public function getLoginForm()
    {
        require_once 'views/auth/login.php';
    }

    public function post()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $param = [
            'username' => $username,
            'password' => $password,
        ];

        $valid = $this->validation($param);

        if ($valid == 0) {
            $_SESSION['error']          = 1;
            $_SESSION['error_messages'] = $this->errorMessages;
            $_SESSION['input']          = $param;
            header('Location: ' . '/auth/login');
        } else {
            $users = Users::login($username, $password);

            if ($users) {
                $_SESSION["login"] = 1;
                $_SESSION["user"]  = $users;
                header('Location: ' . '/');
            } else {
                $_SESSION['error']                     = 1;
                $_SESSION['error_messages']['default'] = 'Combination Username and Password Wrong';
                header('Location: ' . '/auth/login');
            }
        }
    }

    /**
     * [validation input]
     * @param  array  $param [username, password]
     * @return [type]        [description]
     */
    public function validation($param = [])
    {
        $valid = 1;
        // is username valid
        if ($param['username'] == '') {
            $valid                           = 0;
            $this->errorMessages['username'] = "Required";
        } else {
            $countUsername = Users::getCount('username', $param['username']);
            if ($countUsername <= 0) {
                $valid                           = 0;
                $this->errorMessages['username'] = "Username Not Found";
            }
        }

        // is password valid
        if ($param['password'] == '') {
            $valid                           = 0;
            $this->errorMessages['password'] = "Required";
        }
        return $valid;
    }
}
