<?php
define('DB_TABLE', 'users'); // Database name
class Users
{
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    // public $id;
    // public $email;
    // public $username;
    // public $password;

    public static function getAll()
    {
        $db  = Db::getInstance();
        $req = $db->query('SELECT * FROM users');

        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function find($id)
    {
        $db = Db::getInstance();
        // we make sure $id is an integer
        $id  = intval($id);
        $req = $db->prepare('SELECT * FROM posts WHERE id = :id');
        // the query was prepared, now we replace :id with our actual $id value
        $req->execute(['id' => $id]);
        $post = $req->fetch();

        return new Post($post['id'], $post['author'], $post['content']);
    }

    public static function create($param = [])
    {
        try {
            $db         = Db::getInstance();
            $prepareSql = 'INSERT INTO ' . DB_TABLE . '(username, email, password, last_login, created_at, updated_at) VALUES(:username, :email, :password, :last_login, :created_at, :updated_at)';

            $param['last_login'] = date("Y-m-d H:i:s");
            $param['created_at'] = date("Y-m-d H:i:s");
            $param['updated_at'] = date("Y-m-d H:i:s");

            $req = $db->prepare($prepareSql);
            $req->execute($param);

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public static function getByUsername($username = '')
    {
        $db         = Db::getInstance();
        $prepareSql = 'SELECT *  FROM ' . DB_TABLE . ' WHERE username = :username';

        $req = $db->prepare($prepareSql);
        $req->execute(['username' => $username]);

        return $req->fetch(PDO::FETCH_ASSOC);
    }

    public static function login($username = '', $password = '')
    {
        $db     = Db::getInstance();
        $result = self::getByUsername($username);

        if (password_verify($password, $result['password'])) {
            $updateQuery = 'UPDATE ' . DB_TABLE . ' SET last_login = :last_login WHERE id = :id';
            $query       = $db->prepare($updateQuery);
            $query->execute([
                'last_login' => date("Y-m-d H:i:s"),
                'id'         => $result['id'],
            ]);

            return self::getByUsername($username);
        } else {
            return false;
        }
    }

    public static function getCount($column = 'username', $value = '')
    {
        $db         = Db::getInstance();
        $prepareSql = 'SELECT * FROM ' . DB_TABLE . ' WHERE ' . $column . ' = :param';

        $req = $db->prepare($prepareSql);
        $req->execute(['param' => $value]);

        $result = $req->rowCount();

        return $result;
    }
}
