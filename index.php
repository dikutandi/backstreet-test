<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Bangkok");

require_once 'database.php';
$uri = $_SERVER['REQUEST_URI'];

require_once 'routes.php';

if (array_key_exists($uri, $route)) {
    require_once 'controllers/' . $route[$uri]['controller'] . '.php';
    $instanceController = new $route[$uri]['controller'];
    $instanceController->{$route[$uri]['method']}();
} else {
    require_once 'views/pages/error.php';
}
