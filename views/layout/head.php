<DOCTYPE html>
<html>
  <head>
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link href="/assets/css/style.css" rel="stylesheet" id="bootstrap-css">

      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body class="<?php echo isset($_SESSION["login"]) ? 'home' : ''; ?>">
