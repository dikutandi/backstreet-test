<?php
    require_once 'views/layout/head.php';
    $html = '';
    if (isset($_SESSION['error'])) {
        foreach ($_SESSION['error_messages'] as $key => $value) {
            $html .= "<li>$value</li>";
        }
    }
?>
<div class="container">
    <div class="card card-container">
        <img src="/assets/image/logo.png" class="text-center" />
        <?php if ($html != '') {?>
          <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              <?php echo $html; ?>
            </ul>
          </div>
        <?php }?>
        <form class="form-signin" method="post" action="/auth/login/post">
            <label for="username"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" class="form-control"  name="username" value="<?php echo isset($_SESSION['input']) ? $_SESSION['input']['username'] : ''; ?>" required autofocus>

            <label for="password"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" class="form-control"  name="password" required>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
            <a href="/auth/register" class="forgot-password">
                Dont Have Account, Register Now!
            </a>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->

<?php
    require_once 'views/layout/foot.php';
session_destroy();
?>