<?php
    require_once 'views/layout/head.php';
    $html = '';
    if (isset($_SESSION['error'])) {
        foreach ($_SESSION['error_messages'] as $key => $value) {
            $html .= "<li>$value</li>";
        }
    }
?>
<div class="container">
	<div class="card card-container">
        <img src="/assets/image/logo.png" class="text-center" />
        <?php if ($html != '') {?>
          <div class="alert alert-danger alert-dismissible">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <ul>
              <?php echo $html; ?>
            </ul>
          </div>
        <?php }?>
        <form class="form-signin" method="post" action="/auth/register/post">
            <label for="uname"><b>Username</b></label>
	      	<input type="text" placeholder="Enter Username" class="form-control"  name="username" value="<?php echo isset($_SESSION['input']) ? $_SESSION['input']['username'] : ''; ?>" required>

	      	<label for="email"><b>Email</b></label>
	      	<input type="email" placeholder="Enter email" class="form-control"  name="email" value="<?php echo isset($_SESSION['input']) ? $_SESSION['input']['email'] : ''; ?>" required>

	      	<label for="password"><b>Password</b></label>
	      	<input type="password" placeholder="Enter Password" class="form-control"  name="password" required>

	      	<label for="confirm_password"><b>Confirm Password</b></label>
	      	<input type="password" placeholder="Confirm Password" class="form-control"  name="confirm_password" required>


            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Register</button>
            <a href="/auth/login" class="forgot-password">
                Already Have Account, Login Here!
            </a>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->
 <?php
     require_once 'views/layout/foot.php';
     session_destroy();
 ?>
