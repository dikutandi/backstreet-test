<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Users Data</h3></div>
            <div class="panel-body">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Last Login</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($users as $key => $value) {
                            ?>
                            <tr>
                              <td><?=$value['username'];?></td>
                              <td><?=$value['email'];?></td>
                              <td><?=$value['last_login'];?></td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
