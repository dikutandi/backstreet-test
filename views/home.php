<?php
    require_once 'views/layout/head.php';
?>
<div class="navbar navbar-default navbar-static-top" role="navigation"> <!--<div class="navbar navbar-default">-->
    <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/" style="padding-top: 5px">
            <img src="/assets/image/logo-home.png" class="text-center" />
        </a>
    </div>

    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="/"><i class="glyphicon glyphicon-home"></i> Home</a></li>
            <li><a href="/users"><i class="glyphicon glyphicon-user"></i> Users</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right" >
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <?=$user['username'];?><b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/auth/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
    </div>
</div>
<div class="container">
    <?php if ($view == false) {
        ?>
        <div class="row">
            <div class="col-md-6 col-md-offset-3"> <!-- Tengah -->
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Welcome,<b> <?=$user['username'];?> </b></h3></div>
                    <div class="panel-body">
                        You Are Login at <b><?=$user['last_login'];?></b>
                    </div>
                </div>
            </div>
        </div>
        <?php

            } else {
                require_once 'views/' . $view;
            }
        ?>
</div>

<div class="navbar navbar-default navbar-fixed-bottom">
<div class="container">
    <p class="navbar-text pull-left">© Backstreet Test
    <!--
    <a href="http://tinyurl.com/tbvalid" target="_blank" >HTML 5 Validation</a>
    </p>

    <a href="http://youtu.be/zJahlKPCL9g" class="navbar-btn btn-danger btn pull-right">
    <span class="glyphicon glyphicon-star"></span>  Subscribe on YouTube</a>
    -->
</div>
</div>
<?php
require_once 'views/layout/foot.php';
?>