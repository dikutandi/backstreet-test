<?php

class FormValidator
{
    public static $regexes = [
        'date'        => "^[0-9]{4}[-/][0-9]{1,2}[-/][0-9]{1,2}\$",
        'amount'      => "^[-]?[0-9]+\$",
        'number'      => "^[-]?[0-9,]+\$",
        'alfanum'     => "^[0-9a-zA-Z ,.-_\\s\?\!]+\$",
        'not_empty'   => "[a-z0-9A-Z]+",
        'words'       => "^[A-Za-z]+[A-Za-z \\s]*\$",
        'phone'       => "^[0-9]{10,11}\$",
        'zipcode'     => "^[1-9][0-9]{3}[a-zA-Z]{2}\$",
        'plate'       => "^([0-9a-zA-Z]{2}[-]){2}[0-9a-zA-Z]{2}\$",
        'price'       => "^[0-9.,]*(([.,][-])|([.,][0-9]{2}))?\$",
        '2digitopt'   => "^\d+(\,\d{2})?\$",
        '2digitforce' => "^\d+\,\d\d\$",
        'anything'    => "^[\d\D]{1,}\$",
        'username'    => "^[\w]{3,32}\$",
    ];

    private $validations, $sanatations, $mandatories, $equal, $errors, $corrects, $fields;

}
