<?php

define('DB_HOST', '127.0.0.1'); // Database host name (ex. localhost)
define('DB_USER', 'homestead'); // Database user. (ex. root)
define('DB_PASSWORD', 'secret'); // user password  (if no password keep it empty )
define('DB_DATABASE', 'backstreet'); // Database name

class Db
{
    private static $instance = null;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $pdoOptions[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            self::$instance                = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD, $pdoOptions);
        }

        return self::$instance;
    }
}
